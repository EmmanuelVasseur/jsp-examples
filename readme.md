# Bootstrap du projet

Je me suis inspir� de cet exemple pour cr�er le projet et je l'ai adapt� :
http://crunchify.com/simplest-spring-mvc-hello-world-example-tutorial-spring-model-view-controller-tips/

J'ai modifi� l'emplacement de fichier conform�ment au conventions maven :
http://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html
* les sources java sont dans src/main/java
* les ressources web sous dans src/main/webapp

L'architecture de springMvc :
https://www.java4s.com/spring-mvc/spring-mvc-execution-flow-diagram-spring-mvc-3-2-flow/
* Il y a 1 seule servlet 'dispatcher' qui redirige la requ�te http vers le bon controlleur en fonction de l'URL
* Spring utilise par convention des annotations. C'est l'annotation @RequestMapping qui permet de mapper une URL sur un controlleur
* Le controlleur retourne un identifiant de JSP avec �ventuellement un 'mod�le', un ensemble de cl�-valeur utilisables dans les JSP.
* Spring via son ViewResolver fait un redirect vers la JSP correspondante.

Cette unique servlet 'dispatcher' est d�finie dans src/main/webapp/WEB-INF/web.xml.

Par convention le 'contexte' spring associ� est dans 'nom-de-la-servlet'-servlet.xml, soit springMvc-servlet.xml dans notre exemple.

Ce fichier contient la configuration sp�cifique du projet. Spring permet par d�faut d'exposer des services REST. C'est pour cela qu'on a ajout� un ViewResolver, qui d�finit des vues JSP au lieu des vues REST/JSON pr�sentes par d�faut.

Il est indispensable d'utiliser cette servlet 'dispatcher' pour int�grer spring security.

La migration depuis des servlet classiques n'est pas compliqu�e, il faut utiliser le view resolver et transformer les servlets en controlleur en utilisant toutes les annotations disponibles dans springMvc.


# Upload de fichiers

Plusieurs exemples ont �t� ajout�s (upload simple, upload avec champs de formulaire, upload multiples).

Ces exemples ont �t� inspir�s de celui-ci : https://www.journaldev.com/2573/spring-mvc-file-upload-example-single-multiple-files 

Cela ne fonctionne bien que lorsque l'on d�finit le 'multipartResolver' � 'CommonsMultipartResolver' dans src/main/webapp/WEB-INF/springMvc-servlet.xml (avec les d�pendances commons-io et commons-fileupload dans le pom.xml)

# Spring Security

Il s'agit d'un 'filter' http que l'on d�clare dans le web.xml.

Ce 'filter' peut se configurer dans un fichier XML spring.

Dans ce projet exemple c'est le fichier src/main/webapp/WEB-INF/security-config.xml

On y d�finit les autorisations n�cessaires pour acc�der au url et la configuration permettant de r�cup�rer les utilisateurs et les r�les.


# Qualit� de code avec SonarQube

T�l�charger et d�zipper SonarQube sur https://www.sonarqube.org/downloads/
Lancer SonarQube via bin/windows-x86-64/StartSonar.bat

V�rifier que SonarQube est up sur la page http://localhost:9000

Pour lancer une analyse du projet, ex�cuter dans le dossier du projet :
```
mvn clean package
mvn sonar:sonar
```
Ou depuis eclipse dans le menu d'ex�cution

Le r�sultat est pr�sent sur http://localhost:9000/projects?sort=-analysis_date
