package com.evasseur.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Handles requests for the application file upload requests
 */
@Controller
public class FileUploadWithFieldsController {

	@RequestMapping("/upload")
	public String upload() {
		return "upload";
	}

	@RequestMapping("/uploadWithFields")
	public String uploadWithFields() {
		return "uploadWithFields";
	}

	@RequestMapping("/uploadMultiple")
	public String uploadMultiple() {
		return "uploadMultiple";
	}

	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFileProcess", method = RequestMethod.POST)
	@ResponseBody
	public String uploadFileHandler(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file) {

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				System.out.println("Server File Location="
						+ serverFile.getAbsolutePath());

				return "You successfully uploaded file=" + name;
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + name
					+ " because the file was empty.";
		}
	}

	/**
	 * Upload file with user fields
	 */
	@RequestMapping(value = "/uploadFileWithFieldsProcess", method = RequestMethod.POST)
	@ResponseBody
	public String uploadFileWithFieldsHandler(@RequestParam("avatar") MultipartFile avatar,
			@RequestParam("firstname") String firstname,
			@RequestParam("lastname") String lastname,
			@RequestParam("gender") String gender) {

		System.out.println("firstname=" + firstname);
		System.out.println("lastname=" + lastname);
		System.out.println("gender=" + gender);
		return uploadFileHandler(firstname + " " + lastname, avatar);
	}

	/**
	 * Upload multiple file using Spring Controller
	 */
	@RequestMapping(value = "/uploadMultipleFileProcess", method = RequestMethod.POST)
	@ResponseBody
	public String uploadMultipleFileHandler(@RequestParam("name1") String name1,
			@RequestParam("file1") MultipartFile file1, @RequestParam("name2") String name2,
			@RequestParam("file2") MultipartFile file2) {

		return uploadFileHandler(name1,file1) + "\n" + uploadFileHandler(name2,file2);
	}
}