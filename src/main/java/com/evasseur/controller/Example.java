package com.evasseur.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Example {
 
	@RequestMapping("/example")
	public ModelAndView helloWorld() {
 
		String message = "Hello World, this message is coming from Example.java";
		return new ModelAndView("example", "message", message);
	}
}