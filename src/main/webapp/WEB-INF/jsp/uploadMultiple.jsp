<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<title>Upload Multiple File Request Page</title>
</head>
<body>
	<form method="POST" action="uploadMultipleFileProcess" enctype="multipart/form-data">
		File1 to upload: <input type="file" name="file1"><br>
 
		Name1: <input type="text" name="name1"><br>
 
 
		File2 to upload: <input type="file" name="file2"><br>
 
		Name2: <input type="text" name="name2"><br>
 

		<input type="submit" value="Upload"> Press here to upload the file!
	</form>
</body>
</html>